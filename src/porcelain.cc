/*
 * Copyright (c) 2020, Kiyoshi Aman.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the <organization> nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "porcelain.hh"

Porcelain::Porcelain(QWidget *parent) : QWidget(parent) {
	layBar     = new QHBoxLayout();
	layApp     = new QVBoxLayout();
	viewWeb    = new QWebView();
	container  = new QWidget(this);
	btnBack    = new QToolButton();
	btnForward = new QToolButton();
	btnReload  = new QToolButton();
	editURL    = new QLineEdit();

	QAction *actBack = new QAction(btnBack);
	QAction *actForward = new QAction(btnForward);
	QAction *actReload = new QAction(btnReload);

	actBack->setIcon(QIcon::fromTheme("go-previous", QIcon(":icons/go-previous.svg")));
	actForward->setIcon(QIcon::fromTheme("go-next", QIcon(":icons/go-next.svg")));
	actReload->setIcon(QIcon::fromTheme("view-refresh", QIcon(":icons/view-refresh.svg")));

	connect(actBack, &QAction::triggered, this, &Porcelain::goBack);
	connect(actForward, &QAction::triggered, this, &Porcelain::goForward);
	connect(actReload, &QAction::triggered, viewWeb, &QWebView::reload);

	btnBack->setDefaultAction(actBack);
	btnForward->setDefaultAction(actForward);
	btnReload->setDefaultAction(actReload);

	container->setMinimumWidth(600);
	viewWeb->setMinimumSize(600, 400);

	layBar->addWidget(btnBack);
	layBar->addWidget(btnForward);
	layBar->addWidget(btnReload);
	layBar->addWidget(editURL);
	container->setLayout(layBar);
	container->adjustSize();

	layApp->addWidget(container);
	layApp->addWidget(viewWeb);
	setLayout(layApp);
	adjustSize();

	connect(editURL, &QLineEdit::editingFinished, this, &Porcelain::navigateURL);
	connect(viewWeb, &QWebView::urlChanged, this, &Porcelain::updateURL);
}
Porcelain::~Porcelain() {}

void Porcelain::goBack(bool checked) {
	if (!btnForward->isEnabled()) {
		btnForward->setEnabled(true);
	}
	viewWeb->back();
	viewWeb->setFocus(Qt::OtherFocusReason);
}

void Porcelain::goForward(bool checked) {
	if (!btnBack->isEnabled()) {
		btnBack->setEnabled(true);
	}
	viewWeb->forward();
	viewWeb->setFocus(Qt::OtherFocusReason);
}

void Porcelain::loadURL(QString data) {
	editURL->setText(data);
	editURL->editingFinished();
}

void Porcelain::navigateURL() {
	viewWeb->load(QUrl(this->editURL->text()));
	if (viewWeb->history()->canGoBack()) {
		btnBack->setEnabled(true);
	}
	btnForward->setEnabled(false);
	viewWeb->setFocus(Qt::OtherFocusReason);
}

void Porcelain::updateURL(const QUrl &url) {
	editURL->setText(url.url());
	if (!viewWeb->history()->canGoBack()) {
		btnBack->setEnabled(false);
	} else {
		btnBack->setEnabled(true);
	}
	if (!viewWeb->history()->canGoForward()) {
		btnForward->setEnabled(false);
	} else {
		btnForward->setEnabled(true);
	}
}