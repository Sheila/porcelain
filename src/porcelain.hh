
#ifndef PORCELAIN_H_GUARD
#define PORCELAIN_H_GUARD

#include <QMainWindow>
#include <QToolButton>
#include <QLineEdit>
#include <QtWebKitWidgets>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>
#include <QUrl>
#include <QSizePolicy>

class Porcelain : public QWidget {
public:
	Porcelain(QWidget *parent = nullptr);
	~Porcelain();

	void navigateURL();
	void updateURL(const QUrl &url);
	void loadURL(QString data);
	void goBack(bool checked);
	void goForward(bool checked);

private:
	QHBoxLayout *layBar;
	QVBoxLayout *layApp;
	QWidget *container;
	QWebView *viewWeb;
	QToolButton *btnBack;
	QToolButton *btnForward;
	QToolButton *btnReload;
	QLineEdit *editURL;
};

#endif